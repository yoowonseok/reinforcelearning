﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    public NavMeshAgent navMeshAgent = null;
    public Transform player = null;
    public Transform[] waypoints;
    int m_CurrentWaypointIndex;
    public bool isPlayer = false;

    void Start()
    {
        navMeshAgent=GetComponent<NavMeshAgent>();
        navMeshAgent.SetDestination(waypoints[0].position);
    }
    // Update is called once per frame
    void Update()
    {
        if (isPlayer) 
        {
            Vector3 direction = player.position - transform.position;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            if (Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == player)
                {
                    navMeshAgent.SetDestination(player.transform.position);
                }
            }
        }
        else
        {
            if (navMeshAgent.remainingDistance < 0.05f)
            {
                m_CurrentWaypointIndex = Random.Range(0, waypoints.Length-1);
                navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
            }
        }
        
    }
}
