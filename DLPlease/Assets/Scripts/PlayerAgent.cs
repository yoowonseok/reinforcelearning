﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class PlayerAgent : Agent
{
    public Transform pivotTransform;
    public Transform Player;
    public CharacterController PlayerCharacterController;
    public Transform Enemy;
    public EnemyMovement enemyMovement;
    public Transform Cheese;
    public bool getCaught;
    
    private Vector3 move;
    private Vector3 rotation;
    float distanceToCheese;
    float distanceToEnemy;

    void Update()
    {
        getCaught = enemyMovement.isPlayer;
        distanceToCheese = Vector3.Distance(Player.position, Cheese.position);
        distanceToEnemy = Vector3.Distance(Player.position, Enemy.position);
    }

    public override void AgentReset()
    {
        Player.position = new Vector3(Random.Range(-6f,6f),0.5f,Random.Range(-6f,6f));
        Enemy.position = new Vector3(Random.Range(-6f,6f),0.5f,Random.Range(-6f,6f));
        Cheese.position = new Vector3(Random.Range(-6f,6f),0.5f,Random.Range(-6f,6f));
    }

    public float _speed = 5;
    public float _rotationSpeed = 90;
    
    public override void CollectObservations()
    {
        // Target and Agent positions
        AddVectorObs(Player.position);
        AddVectorObs(Player.rotation);

        AddVectorObs(Cheese.position);
        
        AddVectorObs(Enemy.position);
        AddVectorObs(Enemy.rotation);
        
        AddVectorObs(getCaught);

        AddVectorObs(PlayerCharacterController.velocity.x);
        AddVectorObs(PlayerCharacterController.velocity.z);

        AddVectorObs(distanceToCheese);
        AddVectorObs(distanceToEnemy);
                                    
    }
    public override void AgentAction(float[] vectorAction, string textAction)
    {
        AddReward(-0.0001f); //가만히 있는 것에 대한 패널티
        
        this.rotation = new Vector3(0, vectorAction[1]* _rotationSpeed * Time.deltaTime, 0);

        move = new Vector3(0, 0, vectorAction[0] * Time.deltaTime);
        move = this.transform.TransformDirection(move);

        PlayerCharacterController.Move(move * _speed);
        this.transform.Rotate(this.rotation);
        
        
        if (distanceToCheese < 1f)
        {
            SetReward(1.0f);
        }

        if (distanceToEnemy < 1f)
        {
            SetReward(-1.0f);
            Done();
        }

        if(getCaught)
        {
            SetReward(-0.3f);
        }
    }

    /*public override float[] Heuristic()
    {
        var action = new float[2];
        action[0] = Input.GetAxis("Horizontal");
        action[1] = Input.GetAxis("Vertical");
        return action;
    }*/
}
