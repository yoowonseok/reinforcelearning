﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public CharacterController _controller;
    public float _speed = 5;
    public float _rotationSpeed = 90;
    public float gravity=20;

    private Vector3 move;
    private Vector3 rotation;
    void Start()
    {
        move = Vector3.zero;
        _controller = GetComponent<CharacterController>();
    }

    void FixedUpdate()
    {
        if (_controller.isGrounded)
        {
            this.rotation = new Vector3(0, Input.GetAxisRaw("Horizontal") * _rotationSpeed * Time.deltaTime, 0);

            move = new Vector3(0, 0, Input.GetAxisRaw("Vertical") * Time.deltaTime);
            move = this.transform.TransformDirection(move);
            
        }
        move.y -= gravity * Time.deltaTime;
        _controller.Move(move * _speed);
        this.transform.Rotate(this.rotation);
    }
}
