﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class EnemyAgent : Agent
{
    public Transform pivotTransform;
    public Transform Chaser;
    public Transform Cheese;
    public CharacterController PlayerCharacterController;
    public Transform Runner;
    public bool chasing;
    public CirSector fov;

    private Vector3 move;
    private Vector3 rotation;
     float distanceToRunner;

    void Update()
    {
        chasing = fov.isCollision;
        if(chasing)
        {
            distanceToRunner = Vector3.Distance(Chaser.position, Runner.position);
        }
        else
        {
            distanceToRunner = 30f;
        } 
    }

    public override void AgentReset()
    {
        Chaser.position = new Vector3(Random.Range(-6f,6f),0.5f,Random.Range(-6f,6f));
        Runner.position = new Vector3(Random.Range(-6f,6f),0.5f,Random.Range(-6f,6f));
        Cheese.position = new Vector3(Random.Range(-6f,6f),0.5f,Random.Range(-6f,6f));
    }

    public float _speed = 5;
    public float _rotationSpeed = 90;
    
    public override void CollectObservations()
    {
        // Target and Agent positions
        
        AddVectorObs(Chaser.position);
        AddVectorObs(Chaser.rotation);
        
        AddVectorObs(chasing);

        AddVectorObs(PlayerCharacterController.velocity.x);
        AddVectorObs(PlayerCharacterController.velocity.z);

        AddVectorObs(distanceToRunner);
                                    
    }
    public override void AgentAction(float[] vectorAction, string textAction)
    {
        AddReward(-0.0001f); //가만히 있는 것에 대한 패널티
        
        this.rotation = new Vector3(0, vectorAction[1]* _rotationSpeed * Time.deltaTime, 0);

        move = new Vector3(0, 0, vectorAction[0] * Time.deltaTime);
        move = this.transform.TransformDirection(move);

        PlayerCharacterController.Move(move * _speed);
        this.transform.Rotate(this.rotation);
        
        if (distanceToRunner < 1f)
        {
            SetReward(1.0f);
            Done();
        }

        if(chasing)
        {
            SetReward(0.5f);
        }
    }
}
